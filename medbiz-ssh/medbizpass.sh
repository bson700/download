#!/bin/sh

MEDBIZ_SUB_CMD_ARG_PASSWORD=$MEDBIZ_PASSWORD
MEDBIZ_SUB_CMD_ARG_HOST_LIST="--prefix 172.16.110. 243 244 245 246"

MEDBIZ_FILEDEPLOY="medbiz-filedeploy-n.sh"
MEDBIZ_SSH="medbiz-ssh-n.sh"
MEDBIZ_SSH_KEYSHARE="medbiz-ssh-keyshare-n.sh"
MEDBIZ_HOSTADD="medbiz-hostadd-n.sh"
MEDBIZ_USERADD="medbiz-useradd-n.sh"

MEDBIZ_MIN_ARG_COUNT=1
MEDBIZ_SUB_CMD_INDEX=1
MEDBIZ_SUB_CMD_1ST_ARG_INDEX=2
MEDBIZ_SUB_CMD_2ND_ARG_INDEX=$(expr ${MEDBIZ_SUB_CMD_1ST_ARG_INDEX} + 1)
MEDBIZ_SUB_CMD_3ND_ARG_INDEX=$(expr ${MEDBIZ_SUB_CMD_2ND_ARG_INDEX} + 1)

if [ $# -lt ${MEDBIZ_MIN_ARG_COUNT} ]; then
  echo "  Usage1: medpass medbiz-filedeploy-n.sh extra-args..."
  echo "  Usage3: medpass medbiz-ssh-n.sh extra-args..."
  echo "  Usage4: medpass medbiz-ssh-keyshare-n.sh extra-args..."
  echo "  Usage2: medpass medbiz-hostadd-n.sh extra-args..."
  echo "  Usage5: medpass medbiz-useradd-n.sh extra-args..."
fi

medbiz_sub_cmd=${!MEDBIZ_SUB_CMD_INDEX} # ex) "medbiz-ssh.sh"
medbiz_sub_cmd_line="${medbiz_sub_cmd}"

if [ ${medbiz_sub_cmd} == ${MEDBIZ_FILEDEPLOY} ]; then

  medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${MEDBIZ_SUB_CMD_ARG_PASSWORD}"
  for ((i=${MEDBIZ_SUB_CMD_1ST_ARG_INDEX}; i<=$#; i++)); do
    medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${!i}"
  done

elif [ ${medbiz_sub_cmd} == ${MEDBIZ_SSH} ]; then

  medbiz_sub_sub_cmd_line=${!MEDBIZ_SUB_CMD_1ST_ARG_INDEX} # ex) "ls -al"
  medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${MEDBIZ_SUB_CMD_ARG_PASSWORD} \"${medbiz_sub_sub_cmd_line}\""
  for ((i=${MEDBIZ_SUB_CMD_3ND_ARG_INDEX}; i<=$#; i++)); do
    medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${!i}"
  done

elif [ ${medbiz_sub_cmd} == ${MEDBIZ_SSH_KEYSHARE} ]; then

  medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${MEDBIZ_SUB_CMD_ARG_PASSWORD}"
  for ((i=${MEDBIZ_SUB_CMD_2ND_ARG_INDEX}; i<=$#; i++)); do
    medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${!i}"
  done  

elif [ ${medbiz_sub_cmd} == ${MEDBIZ_HOSTADD} ]; then

  for ((i=${MEDBIZ_SUB_CMD_1ST_ARG_INDEX}; i<=$#; i++)); do
    medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${!i}"
  done

elif [ ${medbiz_sub_cmd} == ${MEDBIZ_USERADD} ]; then

  for ((i=${MEDBIZ_SUB_CMD_1ST_ARG_INDEX}; i<=$#; i++)); do
    medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${!i}"
  done

fi

medbiz_sub_cmd_line="${medbiz_sub_cmd_line} ${MEDBIZ_SUB_CMD_ARG_HOST_LIST}"

echo ./${medbiz_sub_cmd_line}

./${medbiz_sub_cmd_line}
