#!/bin/sh

curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbiz-ssh-keyshare-n.sh
curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbiz-hostadd-n.sh
curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbiz-useradd-n.sh
curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbiz-ssh-n.sh
curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbiz-filedeploy-n.sh
curl -LO https://gitlab.com/bson700/download/raw/master/medbiz-ssh/medbizpass.sh

chmod u+x medbiz-ssh-keyshare-n.sh
chmod u+x medbiz-hostadd-n.sh
chmod u+x medbiz-useradd-n.sh
chmod u+x medbiz-ssh-n.sh
chmod u+x medbiz-filedeploy-n.sh
chmod u+x medbizpass.sh

ls -l medbiz-*.sh
