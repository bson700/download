#!/bin/sh

MEDBIZ_PASSWORD=$1
MEDBIZ_USERNAME=$(whoami)
MEDBIZ_THIS_HOSTNAME=$(hostname)
MEDBIZ_THIS_IP=$(hostname -i | awk '{print $NF}')

medbiz_ip_prefix=""
medbiz_hostname_postfix=""
medbiz_host_index=2

if [ $# -lt $medbiz_host_index ]; then
  echo "  Usage1: medbiz-ssh-keyshare.sh password-of-me [--prefix ip-prefix] extra-ip..."
  echo "  Usage2: medbiz-ssh-keyshare.sh password-of-me [--postfix hostname-postfix] extra-hostname..."
  echo "  Usage3: medbiz-ssh-keyshare.sh password-of-me ip..."
  echo "  Usage4: medbiz-ssh-keyshare.sh password-of-me hostname..."
  echo "  Example1: medbiz-ssh-keyshare.sh mypassword --prefix 172.16.110. 243 244 245 246"
  echo "  Example2: medbiz-ssh-keyshare.sh mypassword --postfix .medbiz.or.kr tda1 tda2 tda3 tda4"
  echo "  Example3: medbiz-ssh-keyshare.sh mypassword 172.16.110.243 172.16.110.244 172.16.110.245 172.16.110.246"
  echo "  Example4: medbiz-ssh-keyshare.sh mypassword tda1.medbiz.or.kr tda2.medbiz.or.kr tda3.medbiz.or.kr tda4.medbiz.or.kr"
  echo "  Note: Note the order of the arguments."
  exit 1
fi

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--prefix" ]; then
    next_i=$(expr $i + 1);
    medbiz_ip_prefix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--postfix" ]; then
    next_i=$(expr $i + 1);
    medbiz_hostname_postfix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

medbiz_host=${MEDBIZ_THIS_IP}
medbiz_userhost=${MEDBIZ_USERNAME}@${medbiz_host}
echo \*\*\*\*\* Generating key at ${medbiz_userhost} \(This host\)
yes y | ssh-keygen -t rsa -b 4096 -N '' -f ~/.ssh/id_rsa > /dev/null
exitcode=$?
if [ ${exitcode} -eq 0 ]; then
  echo Succeeded
  cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys
else
  echo Failed
fi

for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost="${MEDBIZ_USERNAME}@${medbiz_host}"
  echo \*\*\*\*\* Generating key at ${medbiz_userhost}
  if [ ${medbiz_host} != $MEDBIZ_THIS_IP ] && [ ${medbiz_host} != $MEDBIZ_THIS_HOSTNAME ]; then
    sshpass -p ${MEDBIZ_PASSWORD} ssh -o StrictHostKeyChecking=no ${medbiz_userhost} "yes y | ssh-keygen -t rsa -b 4096 -N '' -f ~/.ssh/id_rsa" > /dev/null
    exitcode=$?
    if [ ${exitcode} -eq 0 ]; then
      echo Succeeded
      sshpass -p ${MEDBIZ_PASSWORD} ssh -o StrictHostKeyChecking=no ${medbiz_userhost} "cat ~/.ssh/id_rsa.pub" >> ~/.ssh/authorized_keys
    else
      echo Failed
	  exit 1
    fi
  else
    echo Skipped
  fi
done

echo \*\*\*\*\* Sharing keys
for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost=${MEDBIZ_USERNAME}@${medbiz_host}
  sshpass -p ${MEDBIZ_PASSWORD} scp ~/.ssh/authorized_keys ${medbiz_userhost}:~/.ssh
done

echo \*\*\*\*\* Shared keys
cat ~/.ssh/authorized_keys
