#!/bin/sh

MEDBIZ_USERADD_USERNAME=$1
MEDBIZ_USERADD_PASSWORD=$2
MEDBIZ_ROOT_USERNAME=root
MEDBIZ_THIS_HOSTNAME=$(hostname)
MEDBIZ_WHOAMI=$(whoami)

medbiz_ip_prefix=""
medbiz_hostname_postfix=""
medbiz_host_index=3

if [ $# -lt $medbiz_host_index ]; then
  echo "  Usage1: medbiz-useradd.sh username-to-add password-to-add [--prefix ip-prefix] extra-ip..."
  echo "  Usage2: medbiz-useradd.sh username-to-add password-to-add [--postfix hostname-postfix] extra-hostname..."
  echo "  Usage3: medbiz-useradd.sh username-to-add password-to-add ip..."
  echo "  Usage4: medbiz-useradd.sh username-to-add password-to-add hostname..."
  echo "  Example1: medbiz-useradd.sh id-to-add password-to-add --prefix 172.16.110. 243 244 245 246"
  echo "  Example2: medbiz-useradd.sh id-to-add password-to-add --postfix .medbiz.or.kr tda1 tda2 tda3 tda4"
  echo "  Example3: medbiz-useradd.sh id-to-add password-to-add 172.16.110.243 172.16.110.244 172.16.110.245 172.16.110.246"
  echo "  Example4: medbiz-useradd.sh id-to-add password-to-add tda1.medbiz.or.kr tda2.medbiz.or.kr tda3.medbiz.or.kr tda4.medbiz.or.kr"
  echo "  Note: Note the order of the arguments."
  exit 1
fi

if [ $MEDBIZ_WHOAMI != $MEDBIZ_ROOT_USERNAME ]; then
  echo It must be a root account.
fi

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--prefix" ]; then
    next_i=$(expr $i + 1);
    medbiz_ip_prefix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--postfix" ]; then
    next_i=$(expr $i + 1);
    medbiz_hostname_postfix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost=${MEDBIZ_ROOT_USERNAME}@${medbiz_host}
  echo \*\*\*\*\* Adding ${MEDBIZ_USERADD_USERNAME} at ${medbiz_host}
  ssh -o StrictHostKeyChecking=no ${medbiz_userhost} "useradd -m ${MEDBIZ_USERADD_USERNAME}"
  ssh -o StrictHostKeyChecking=no ${medbiz_userhost} "echo '${MEDBIZ_USERADD_USERNAME}:${MEDBIZ_USERADD_PASSWORD}' | chpasswd"
  exitcode=$?
  if [ ${exitcode} -eq 0 ]; then
    echo Succeeded
  else
    echo Failed
    exit 1
  fi
done
