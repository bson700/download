#!/bin/sh

MEDBIZ_THIS_HOSTNAME=$(hostname)
MEDBIZ_WHOAMI=$(whoami)
MEDBIZ_PASSWORD=$1
MEDBIZ_CMDLINE=$2

medbizz_ip_prefix=""
medbiz_hostname_postfix=""
medbiz_host_index=3

if [ $# -lt $medbiz_host_index ]; then
  echo "  Usage1:  medbiz-ssh.sh password \"command-line\" [--prefix ip-prefix] extra-ip..."
  echo "  Usage2:  medbiz-ssh.sh password \"command-line\" [--postfix hostname-postfix] extra-hostname..."
  echo "  Usage3:  medbiz-ssh.sh password \"command-line\" ip..."
  echo "  Usage4:  medbiz-ssh.sh password \"command-line\" hostname..."
  echo "  Example1:  medbiz-ssh.sh password \"ls -l\" --prefix 172.16.110. 243 244 245 246"
  echo "  Example2:  medbiz-ssh.sh password \"ls -l\" --postfix .medbiz.or.kr tda1 tda2 tda3 tda4"
  echo "  Example3:  medbiz-ssh.sh password \"ls -l\" 172.16.110.243 172.16.110.244 172.16.110.245 172.16.110.246"
  echo "  Example4:  medbiz-ssh.sh password \"ls -l\" tda1.medbiz.or.kr tda2.medbiz.or.kr tda3.medbiz.or.kr tda4.medbiz.or.kr"
  echo "  Note: Note the order of the arguments."
  exit 1
fi

for ((i=0; i<=$#; i++)); do
  if [ "${!i}" == "--prefix" ]; then
    next_i=$(expr $i + 1);
    medbiz_ip_prefix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=0; i<=$#; i++)); do
  if [ "${!i}" == "--postfix" ]; then
    next_i=$(expr $i + 1);
    medbiz_hostname_postfix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost=${MEDBIZ_WHOAMI}@${medbiz_host}
  echo \*\*\*\*\* At ${medbiz_host}
  sshpass -p ${MEDBIZ_PASSWORD} ssh -o StrictHostKeyChecking=no ${medbiz_userhost} ${MEDBIZ_CMDLINE}
  exitcode=$?
  if [ ${exitcode} -eq 0 ]; then
    echo Succeeded
  else
    echo Failed
    exit 1
  fi
done
