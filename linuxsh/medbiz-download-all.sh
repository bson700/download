#!/bin/sh

curl -LO https://gitlab.com/bson700/download/raw/master/linuxsh/medbiz-ssh-keyshare.sh
curl -LO https://gitlab.com/bson700/download/raw/master/linuxsh/medbiz-hostadd.sh
curl -LO https://gitlab.com/bson700/download/raw/master/linuxsh/medbiz-useradd.sh
curl -LO https://gitlab.com/bson700/download/raw/master/linuxsh/medbiz-ssh.sh
curl -LO https://gitlab.com/bson700/download/raw/master/linuxsh/medbiz-filedeploy.sh

chmod u+x medbiz-ssh-keyshare.sh
chmod u+x medbiz-hostadd.sh
chmod u+x medbiz-useradd.sh
chmod u+x medbiz-ssh.sh
chmod u+x medbiz-filedeploy.sh

ls -l medbiz-*.sh
