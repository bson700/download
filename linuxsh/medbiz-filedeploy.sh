#!/bin/sh

MEDBIZ_PASSWORD=$1
MEDBIZ_USERNAME=$(whoami)
MEDBIZ_THIS_HOSTNAME=$(hostname)
MEDBIZ_THIS_IP=$(hostname -i | awk '{print $NF}')
MEDBIZ_SRC_FILE=$2
MEDBIZ_DST_DIR=$3

medbiz_ip_prefix=""
medbiz_hostname_postfix=""
medbiz_host_index=4

if [ $# -lt $medbiz_host_index ]; then
  echo "  Usage1: medbiz-filedeploy.sh password-of-me source-file destination-directory [--prefix ip-prefix] extra-ip..."
  echo "  Usage2: medbiz-filedeploy.sh password-of-me source-file destination-directory [--postfix hostname-postfix] extra-hostname..."
  echo "  Usage3: medbiz-filedeploy.sh password-of-me source-file destination-directory ip..."
  echo "  Usage4: medbiz-filedeploy.sh password-of-me source-file destination-directory hostname..."
  echo "  Example1: medbiz-filedeploy.sh mypassword jdk-8u211-linux-x64.rpm \$PWD --prefix 172.16.110. 243 244 245 246"
  echo "  Example2: medbiz-filedeploy.sh mypassword jdk-8u211-linux-x64.rpm \$PWD --postfix .medbiz.or.kr tda1 tda2 tda3 tda4"
  echo "  Example3: medbiz-filedeploy.sh mypassword jdk-8u211-linux-x64.rpm \$PWD 172.16.110.243 172.16.110.244 172.16.110.245 172.16.110.246"
  echo "  Example4: medbiz-filedeploy.sh mypassword jdk-8u211-linux-x64.rpm \$PWD tda1.medbiz.or.kr tda2.medbiz.or.kr tda3.medbiz.or.kr tda4.medbiz.or.kr"
  echo "  Note: Note the order of the arguments."
  exit 1
fi

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--prefix" ]; then
    next_i=$(expr $i + 1);
    medbiz_ip_prefix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--postfix" ]; then
    next_i=$(expr $i + 1);
    medbiz_hostname_postfix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost=${MEDBIZ_USERNAME}@${medbiz_host}
  echo \*\*\*\*\* Deploying to ${medbiz_host}
  if [ ${medbiz_host} != $MEDBIZ_THIS_IP ] && [ ${medbiz_host} != $MEDBIZ_THIS_HOSTNAME ]; then
    sshpass -p ${MEDBIZ_PASSWORD} scp ${MEDBIZ_SRC_FILE} ${medbiz_userhost}:${MEDBIZ_DST_DIR}
    exitcode=$?
    if [ ${exitcode} -eq 0 ]; then
      echo Succeeded
    else
      echo Failed
      exit 1
    fi
  else
    echo Skipped
  fi
done
