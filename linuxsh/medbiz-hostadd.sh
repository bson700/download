#!/bin/sh

MEDBIZ_ROOT_USERNAME=root
MEDBIZ_THIS_IP=$(hostname -i | awk '{print $NF}')
MEDBIZ_WHOAMI=$(whoami)

medbiz_ip_prefix=""
medbiz_hostname_postfix=""
medbiz_host_index=1

if [ $# -lt $medbiz_host_index ]; then
  echo "  Usage1: medbiz-hostadd.sh [--prefix ip-prefix] extra-ip..."
  echo "  Usage2: medbiz-hostadd.sh [--postfix hostname-postfix] extra-hostname..."
  echo "  Usage3: medbiz-hostadd.sh ip..."
  echo "  Usage4: medbiz-hostadd.sh hostname..."
  echo "  Example1: medbiz-hostadd.sh --prefix 172.16.110. 243 244 245 246"
  echo "  Example2: medbiz-hostadd.sh --postfix .medbiz.or.kr tda1 tda2 tda3 tda4"
  echo "  Example3: medbiz-hostadd.sh 172.16.110.243 172.16.110.244 172.16.110.245 172.16.110.246"
  echo "  Example4: medbiz-hostadd.sh tda1.medbiz.or.kr tda2.medbiz.or.kr tda3.medbiz.or.kr tda4.medbiz.or.kr"
  echo "  Note: Note the order of the arguments."
  exit 1
fi

if [ $MEDBIZ_WHOAMI != $MEDBIZ_ROOT_USERNAME ]; then
  echo It must be a root account.
fi

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--prefix" ]; then
    next_i=$(expr $i + 1);
    medbiz_ip_prefix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=0; i<=$#; i++)); do
  if [ ${!i} == "--postfix" ]; then
    next_i=$(expr $i + 1);
    medbiz_hostname_postfix=${!next_i}
    medbiz_host_index=$(expr ${medbiz_host_index} + 2)
    break
  fi
done

for ((i=$medbiz_host_index; i<=$#; i++)); do
  medbiz_host="${medbiz_ip_prefix}${!i}${medbiz_hostname_postfix}"
  medbiz_userhost=${MEDBIZ_ROOT_USERNAME}@${medbiz_host}

  echo ""
  echo \*\*\*\*\* Getting \(IP \& Hostname\) from ${medbiz_host}
  medbiz_remote_hostname=$(ssh -o StrictHostKeyChecking=no ${medbiz_userhost} hostname)
  exitcode=$?
  if [ ${exitcode} -ne 0 ]; then
    echo Failed
    exit 1
  fi
  medbiz_remote_ip=$(ssh -o StrictHostKeyChecking=no ${medbiz_userhost} hostname -i | awk '{print $NF}')
  medbiz_remote_host_ip_pair="${medbiz_remote_ip} ${medbiz_remote_hostname}"

  echo \*\*\*\*\* Deploying \(IP \& Hostname\) to all hosts
  for ((j=$medbiz_host_index; j<=$#; j++)); do
    medbiz_host_2="${medbiz_ip_prefix}${!j}${medbiz_hostname_postfix}"
    medbiz_userhost_2=${MEDBIZ_ROOT_USERNAME}@${medbiz_host_2}
    ssh -o StrictHostKeyChecking=no ${medbiz_userhost_2} "echo ${medbiz_remote_host_ip_pair} >> /etc/hosts"
  done
done

for ((k=$medbiz_host_index; k<=$#; k++)); do
  medbiz_host_3="${medbiz_ip_prefix}${!k}${medbiz_hostname_postfix}"
  medbiz_userhost_3=${MEDBIZ_ROOT_USERNAME}@${medbiz_host_3}

  echo \*\*\*\*\* /etc/hosts at ${medbiz_host_3}
  ssh -o StrictHostKeyChecking=no ${medbiz_userhost_3} "tail /etc/hosts"
done
